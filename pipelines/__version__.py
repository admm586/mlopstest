"""Metadata for the pipelines package."""

__title__ = "pipelines"
__description__ = "pipelines - MLOps CreditCard"
__version__ = "0.0.1"
__author__ = "Simon Hui"
__author_email__ = "simon.hui@bendigoadelaide.com.au"
__license__ = "Apache 2.0"
__url__ = "<https://your-website>"
